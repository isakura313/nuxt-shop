import {defineStore} from "pinia";

export const products = defineStore({
    id: 'product-store',
    state: () => {
        return {
            cart: [],
        }
    },
    actions: {
        addProductToCart(product: string) {
            this.cart.push(product)
        },
    },
    getters: {
        filtersList: state => state.filtersList,
    },
})