module.exports = {
    content: ['./src/**/*.{html,js}'],
    theme: {
        colors: {
            'blue': '#1e3799',
            'red': '#eb2f06',
            'orange': '#ff7849',
            'green': '#079992',
            'yellow': '#ffc82c',
            'gray-dark': '#fa983a',
            'gray': '#C1C1C1',
        },
        fontFamily: {
            roboto: ['Roboto', 'sans-serif'],
            serif: ['Merriweather', 'serif'],
        },
        extend: {
            spacing: {
                '8xl': '96rem',
                '9xl': '128rem',
            },
            borderRadius: {
                '4xl': '2rem',
            }
        }
    },
}